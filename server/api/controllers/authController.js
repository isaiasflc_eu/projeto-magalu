const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");

module.exports = app => {
    const clientModel = app.model.clientModel;
    const controller = {};

    controller.authenticate = (req, res, next) => {
        if(req.body.email){
            clientModel.findOne({email: req.body.email}).then(function (result) {
                if(result){
                    let token = jwt.sign({id: result._id}, authConfig.secret, {
                        expiresIn: 3600
                    });
                    let user = result.toJSON();
                    user.token = token;
                    res.status(200).send(user);
                }else{
                    res.status(404).send({message: "usuario nao Encontrado"});
                }
            }).catch(next);
        }else{
            res.status(404).send({message:"o atributo email é obrigatorio"});
        }
        

    };

    return controller;
}