const _ = require('lodash');

module.exports = app => {
    const clientModel = app.model.clientModel;
    const controller = {};

    controller.saveClient = (req, res, next) => {
        if(req.body.email && req.body.name){
            clientModel.create(req.body).then(function (result) {
                res.status(200).send(result);
            }).catch(function (err, next){
                if(err.name === 'MongoError' && err.code === 11000){
                    res.status(422).send({message:"Email já exuste, favor inserir um novo "});
                }else{
                    return next;
                }
            });
        }else{
            res.status(404).send({message:"o atributo email e nome são obrigatorios"});
        }

    };

    controller.listClient = (req, res, next) => {

        clientModel.find({}).then(function (result) {
            res.status(200).send(result);
        }).catch(next);

    };

    controller.getClientByEmail = (req, res, next) => {
        if(req.params.email){
            clientModel.findOne({email: req.params.email}).then(function (result) {
                if(result){
                    res.status(200).send(result);
                }else{
                    res.status(404).send({message: "usuario nao Encontrado"});
                }
            }).catch(next);
        }else{
            res.status(404).send({message:"o atributo email é obrigatorio"});
        }

    };

    controller.updateClient = (req, res, next) => {

        if(req.params.email && req.body){
            clientModel.findOneAndUpdate({ email: req.params.email }, req.body, {new:true})
                .then(function (result) {
                    if(result && req.body.email){
                        
                        res.status(200).send(result);
                        
                    }else{
                        res.status(404).send({message:"Usuario nao encontrado"});
                    }
                }).catch(next);
        }else{
            res.status(404).send({message:"o atributo body ou email estão incorretos"});
        }

    };

    controller.removeClient = (req, res, next) => {
        if(req.params.email){
            clientModel.findOneAndRemove({ email: req.params.email }).then(function (result) {
                if(result){
                    res.status(200).send(result);
                }else{
                    res.status(404).send({message:"Usuario nao encontrado"});
                }
                
            }).catch(next);
        }else{
            res.status(404).send({message:"o atributo email é obrigatorio"});
        }

    };

    controller.favoriteList = (req, res, next) => {

        if(req.params.email && req.body && req.body.id){
            app.controllers.productController.getProductbyIdValidation(req.body.id)
                .then(function(product){
                    if(product.stack){
                        res.status(404).send({message:"esse produto nao existe"});
                        throw new Error('produto nao existe');
                    }else{
                        return clientModel.findOne({ email: req.params.email });
                    }
                    
                }).then(function (result) {
                    if(result){
                        if(_.findIndex(result.favorite_list, {id: req.body.id}) !== -1){

                            res.status(201).send({message:"Produto ja existe na lista de favoritos"});

                        }else{
                            clientModel.findOneAndUpdate({ email: req.params.email }, {$push:{favorite_list: req.body}},{new:true}).then(function (result_one) {
                                res.status(200).send(result_one);
                            });
                        }
                        
                    }else{
                        res.status(404).send({message:"Usuario nao encontrado"});
                    }
                }).catch(next);
        }else{
            res.status(404).send({message:"o atributo body ou email estão incorretos"});
        }

    };

    return controller;
}