const axios = require("axios");

module.exports = app => {
    const controller = {};

    controller.listProductByPage = (req, res, next) => {
        if(req.params.page){
            axios
                .get(`http://challenge-api.luizalabs.com/api/product/?page=${req.params.page}`)
                .then(response => {
                    res.status(200).send(response.data.products);
                })
                .catch(error => {
                    console.error(error)
                    res.status(404).send(error.message);
                })
        }else{
            res.status(404).send("Obrigatorio o numero da Pagina");
        }

    };

    controller.getProductbyId = (req, res, next) => {
        if(req.params.id_product){
            axios
                .get(`http://challenge-api.luizalabs.com/api/product/${req.params.id_product}/`)
                .then(response => {
                    res.status(200).send(response.data);
                })
                .catch(error => {
                    console.error(error)
                    res.status(404).send(error.message);
                });    
        }else{
            res.status(404).send("Obrigatorio o id do produto");
        }

    };

    controller.getProductbyIdValidation = async (id_product) => {
        
        return axios
            .get(`http://challenge-api.luizalabs.com/api/product/${id_product}/`)
            .then(response => {
                return response.data
            })
            .catch(error => {
                return error
            });    


    };

    return controller;
}