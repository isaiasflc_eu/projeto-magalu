const mongoose = require("mongoose");

module.exports = app => {
    const clientSchema = new mongoose.Schema(
        {
            name: {
                type: String
            },
            email: {
                type: String,
                required: true,
                unique: true,
            },
            favorite_list:{
                type: Array,
            }
        },
        {
            timestamps: true
        }
    );

    return mongoose.model("client", clientSchema);
}
