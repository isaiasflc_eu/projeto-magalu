const authMiddleware = require("../middlewares/auth");

module.exports = app => {
    const controller = app.controllers.clientController;
  
    app.route('/client')
      .post(controller.saveClient);
    
    app.use(authMiddleware);
    
    app.route('/client')
      .get(controller.listClient);

    app.route('/client/findemail/:email')
      .get(controller.getClientByEmail)

    app.route('/client/:email')
      .put(controller.updateClient)
      .delete(controller.removeClient);

    app.route('/client/:email/addfavorite')
      .put(controller.favoriteList);
  
}