const authMiddleware = require("../middlewares/auth");

module.exports = app => {
    const controller = app.controllers.productController;
    app.use(authMiddleware);
    //app.route().use(authMiddleware);
    app.route('/product/list/:page')
      .get(controller.listProductByPage);
    
    app.route("/product/findid/:id_product")
      .get(controller.getProductbyId);
  
}