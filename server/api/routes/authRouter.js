module.exports = app => {
    const controller = app.controllers.authController;
  
    app.route('/authenticate')
      .post(controller.authenticate)
  
}