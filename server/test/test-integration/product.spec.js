process.env.NODE_ENV = 'test';
describe('product', function () {
    before(function (done) {
        //Sempre depois de executar o nosso teste, iremos limpar a nossa base de dados:
        ClientModel.deleteMany({}, function (error) {
            done();
        });
        
    });

    /** 
    * Teste das rotas do product
    * devemos primeiro criar um client
    */
    describe('Criar Cliente; Autenticar cliente; Buscar lista de Produtos; buscar produto por id', function () {
        
        /** 
        * Teste da rota: criar cliente
        * devemos primeiro criar um cliente
        */
        it('Deve criar um cliente', function (done) {
            var clientObjc = {
                name: "cliente01",
                email: "isaias@isaias.com"
            }
            chai.request(app)
                .post('/client')
                .send(clientObjc)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name').equal(clientObjc.name); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('email').equal(clientObjc.email); //verifica se apropriedade do objeto criado
                    chai.expect(res.body.favorite_list.length).to.be.equals(0); //verifica se apropriedade do objeto criado
                    done();
                });
        });
    

        /** 
        * Teste da rota: autenticação
        * devemos primeiro criar um cliente
        */
        it('Deve autenticar um cliente e gerar o token', function (done) {
            var clientObjc = {
                name: "cliente01",
                email: "isaias@isaias.com"
            }
            chai.request(app)
                .post('/authenticate')
                .send(clientObjc)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name').equal(clientObjc.name); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('email').equal(clientObjc.email); //verifica se apropriedade do objeto criado
                    chai.expect(res.body.favorite_list.length).to.be.equals(0); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('token'); //verifica se tem a propriedade token
                    global.token = res.body.token;
                    done();
                });
        });
        /** 
        * Teste da rota: /GET
        */
         it('Deve retornar todos os produtos', function (done) {
            
            chai.request(app)
                .get('/product/list/' + 1)
                .set('Authorization', 'Bearer ' + global.token)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    chai.expect(res.body).to.have.lengthOf(100); //verifica se o array esta vazio
                    done();
                });
        });

        /** 
        * Teste da rota: /GET
        */
        it('Deve retornar apenas um produto', function (done) {
            var productObjc = {
                id: "1bf0f365-fbdd-4e21-9786-da459d78dd1f",
                price: 1699,
                image: "http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg",
                brand: "bébé confort",
                title: "Cadeira para Auto Iseos Bébé Confort Earth Brown"
            }
            chai.request(app)
                .get('/product/findid/' + productObjc.id)
                .set('Authorization', 'Bearer ' + global.token)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('id');
                    res.body.should.have.property('title').equal(productObjc.title); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('brand').equal(productObjc.brand); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('price').equal(productObjc.price); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('image').equal(productObjc.image); //verifica se apropriedade do objeto criado
                    done();
                });
        });
    });
});