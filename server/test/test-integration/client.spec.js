process.env.NODE_ENV = 'test';
describe('Client', function () {
    before(function (done) {
        //Sempre depois de executar o nosso teste, iremos limpar a nossa base de dados:
        ClientModel.deleteMany({}, function (error) {
            done();
        });
        
    });

    /** 
    * Teste das rotas do Cliente
    * devemos primeiro criar um cliente
    */
    describe('Criar Cliente; Autenticar cliente; Buscar lista de Cliente; buscar cliente por email; atualizar cliente; remover client; add Produto a lista de favoritos', function () {
        
        /** 
        * Teste da rota: criar cliente
        * devemos primeiro criar um cliente
        */
        it('Deve criar um cliente', function (done) {
            var clientObjc = {
                name: "cliente01",
                email: "isaias@isaias.com"
            }
            chai.request(app)
                .post('/client')
                .send(clientObjc)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name').equal(clientObjc.name); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('email').equal(clientObjc.email); //verifica se apropriedade do objeto criado
                    chai.expect(res.body.favorite_list.length).to.be.equals(0); //verifica se apropriedade do objeto criado
                    done();
                });
        });
    

        /** 
        * Teste da rota: autenticação
        * devemos primeiro criar um cliente
        */
        it('Deve autenticar um cliente e gerar o token', function (done) {
            var clientObjc = {
                name: "cliente01",
                email: "isaias@isaias.com"
            }
            chai.request(app)
                .post('/authenticate')
                .send(clientObjc)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name').equal(clientObjc.name); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('email').equal(clientObjc.email); //verifica se apropriedade do objeto criado
                    chai.expect(res.body.favorite_list.length).to.be.equals(0); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('token'); //verifica se tem a propriedade token
                    global.token = res.body.token;
                    done();
                });
        });
    


        /** 
        * Teste da rota: /GET
        */
        it('Deve retornar todos os clientes', function (done) {
            chai.request(app)
                .get('/client')
                .set('Authorization', 'Bearer ' + global.token)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    chai.expect(res.body.length).to.be.equals(1); //verifica se o array esta vazio
                    done();
                });
        });

        /** 
        * Teste da rota: /GET
        */
        it('Deve retornar apenas um client', function (done) {
            var clientObjc = {
                name: "cliente01",
                email: "isaias@isaias.com"
            }
            chai.request(app)
                .get('/client/findemail/' + clientObjc.email)
                .set('Authorization', 'Bearer ' + global.token)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name').equal(clientObjc.name); //verifica se apropriedade do objeto criado
                    res.body.should.have.property('email').equal(clientObjc.email); //verifica se apropriedade do objeto criado
                    chai.expect(res.body.favorite_list.length).to.be.equals(0); //verifica se apropriedade do objeto criado
                    done();
                });
        });

        it('Deve atualizar um cliente', function (done) {
            let clientObjcUpdate = {
                name: "clienteUpdate",
                email: "isaias@isaias.com"
            }
            
            chai.request(app)
                .put('/client/' + clientObjcUpdate.email)
                .set('Authorization', 'Bearer ' + global.token)
                .send(clientObjcUpdate)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name').equal(clientObjcUpdate.name); //verifica se retornou o objeto criado
                    res.body.should.have.property('email').equal(clientObjcUpdate.email); //verifica se retornou o objeto criado
                    chai.expect(res.body.favorite_list.length).to.be.equals(0); //verifica se retornou o objeto criado
                    done();
                });
        });

        it('Deve adc um produto na lista de favoritos do cliente', function (done) {
            let clientObjcUpdate = {
                name: "clienteUpdate",
                email: "isaias@isaias.com"
            }

            let product = {
                price: 1699,
                image: "http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg",
                brand: "bébé confort",
                id: "1bf0f365-fbdd-4e21-9786-da459d78dd1f",
                title: "Cadeira para Auto Iseos Bébé Confort Earth Brown"
            }
            
            chai.request(app)
                .put('/client/' + clientObjcUpdate.email + "/addfavorite")
                .set('Authorization', 'Bearer ' + global.token)
                .send(product)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name').equal(clientObjcUpdate.name); //verifica se retornou o objeto criado
                    res.body.should.have.property('email').equal(clientObjcUpdate.email); //verifica se retornou o objeto criado
                    chai.expect(res.body.favorite_list.length).to.be.equals(1); //verifica se retornou o objeto criado
                    done();
                });
        });

        it('Deve remover um cliente', function (done) {
            let clientObjcUpdate = {
                name: "clienteUpdate",
                email: "isaias@isaias.com"
            }
            
            chai.request(app)
                .delete('/client/' + clientObjcUpdate.email)
                .set('Authorization', 'Bearer ' + global.token)
                .send(clientObjcUpdate)
                .end(function (error, res) {
                    chai.expect(error).to.be.null; // verifica erro
                    chai.expect(res).to.have.status(200);//verifica status
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name').equal(clientObjcUpdate.name); //verifica se retornou o objeto criado
                    res.body.should.have.property('email').equal(clientObjcUpdate.email); //verifica se retornou o objeto criado
                    chai.expect(res.body.favorite_list.length).to.be.equals(1); //verifica se retornou o objeto criado
                    done();
                });
        });
    });
});
