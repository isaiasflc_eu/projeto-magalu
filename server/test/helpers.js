const chai = require('chai');
const http = require("chai-http");
const subSet = require("chai-subset");
const app = require("../config/express")();

global.app = app;
global.ClientModel = app.model.clientModel;
chai.use(http);
chai.use(subSet);
chai.should();
global.chai = chai;



