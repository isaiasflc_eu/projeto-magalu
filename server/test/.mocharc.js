'use strict'
process.env.NODE_ENV = 'test';
module.exports = {
    require: "test/helpers.js",
    reporter: "spec",
    slow: 5000,
    exit: true
}
