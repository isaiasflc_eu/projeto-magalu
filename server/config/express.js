const express    = require('express');
const bodyParser = require('body-parser');
var cors = require('cors')
const config     = require('config');
const mongoose = require("mongoose");
const consign    = require('consign');


module.exports = () => {
  const app = express();

  // SETANDO VARIÁVEIS DA APLICAÇÃO
  app.set('port', process.env.PORT || config.get('server.port'));

  // MIDDLEWARES
  app.use(express.json());
  app.use(cors());

  // ERROR HANDLING MIDDLEWARE
  app.use(function(err, req, res, next){
    console.log(err);
    // ‘res.status(422)’->muda o status
    res.status(422).send({error: err.message});
  });

  mongoose.set('useNewUrlParser', true);
  mongoose.set('useFindAndModify', false);
  mongoose.set('useCreateIndex', true);

  //CONNECT MONGODB
  mongoose.connect(config.get('dataBase.uri'), { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

  // CONFIRM CONNECTION 
  mongoose.connection.on('connected', function () {
    console.log('Connected to Database');
  });

  // Mensagem de Erro
  mongoose.connection.on('error', (err) => {
    console.log('Database error ' + err);
  });
  

  // ENDPOINTS
  consign({cwd: 'api'})
    .then('model')
    .then('controllers')
    .then('routes')
    .into(app);

  return app;
};