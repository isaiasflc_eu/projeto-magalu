# Project Magalu

Pré - requisitos  para rodar local sem docker: 

	1- Ter instalado na máquina o NodeJS 14.x ou mais.
	2- conexão com a internet e liberação de firewall para o site mongoDB Atlas.
	
Pré - requisitos  para rodar local com docker:

	1- Ter instalado na máquina o NodeJS 14.x ou mais.
	2- conexão com a internet e liberação de firewall para o site mongoDB Atlas.
	3- ter instalado na maquina Docker
	4- possuir uma conta e logar com a mesma no software da docker.

Para executar o projeto localmente sem docker:

	1- entrar na pasta do projeto pelo CMD e executa o comando "npm install"
	2- depois no próprio cmd executar o comando "node server.js"
	3- Aplicação irá subir na porta 8080

Para executar o projeto localmente com docker:

	1- Certifique que esteja logado no software docker e com o status "running"
	2- entra na pasta do projeto pelo Terminal e executa o comando "docker build -t <your username>/projeto-magalu .", substitua o <your username> pelo seu user do docker.
	3- executa o comando "docker run -p 49160:8080 -d <your username>/projeto-magalu", substitua o <your username> pelo seu user do docker.
	4- aplicação irá subir na porta 49160
	
Para rodar os Testes integrados: 
	
	1- certifique-se que instalou todos os módulos com o "npm install"
	2- entre na pasta do projeto pelo terminal e execute o comando "npm run test"

Considerações:

	Usei a linguagem de programação NodeJS e o framework Express para desenvolver a API. Separei as camadas ROUTER, Controller e Model.
	Usei banco de Dados MongoDB e o seu módulo em node mongoose, para mapeamento de tabela e execução de query.
	Usei o "consign" para facilitar a importação das controllers e rotas e models.
	Usei Chai e Mocha para teste Integrado
	usei DockerFile para Dockerizar a aplicação.

Atenção:

	1- Deixei as rotas de "POST /authenticate" e a rota "POST /client sem middleware de autenticação".
	2- Para acessar as demais rotas precisa ser criado um client, autenticar o client e pegar o token gerado e adicionar ele nas outras rotas da api.

Rotas CRUD client:

	GET/ http://localhost:8080/client/findemail/:email  --> Retorna um cliente especifico que corresponde ao email passado.
	GET/ http://localhost:8080/client  --> Lista todos os clientes cadastrado
	POST/ http://localhost:8080/client --> Cria um cliente, precisa passar o json no body, segue exemplo: {"name": "nomeQualquer", "email": "luiza@luiza.com"}.
	
	Para executar o PUT E DELETE precisa ter ao menos um client cadastrado, no retorno do Post favor pegar o email do client, pois o mesmo sera usado nas próximas rota 
		
		PUT/ http://localhost:8080/client/:email  --> substitua o ":email" pelo email do item salvo no POST, passe no body um json, segue exemple: {"name": "nomeQualquer", "email": "luiza@luiza.com"}
		PUT/ http://localhost:8080/client/:email/addfavorite  --> substitua o ":email" pelo email do item salvo no POST, passe no body um json de produto, segue exemple: 
		{
			"price": 1699, 
			"image": "http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg", 
			"brand": "bébé confort", 
			"id": "1bf0f365-fbdd-4e21-9786-da459d78dd1f",
			"title": "Cadeira para Auto Iseos Bébé Confort Earth Brown"
		}

		DELETE/ http://localhost:8080/client/:email -> substitua o ":email" pelo email do item salvo no POST.

Rotas dos produtos: 
	
	POST/ http://localhost:8080/product/list/:page  --> substitua o ":page" pelo numero da pagina que deseja
	PUT/ http://localhost:8080/product/findid/:id_product  --> substitua o ":id_product" pelo numero da pagina que deseja
	
Considerações finais: 

	Bom quero agradecer por participar do teste, espero ter feito tudo que me foi pedido, qualquer dúvida estou a disposição. Gostaria de avisa também que acabei criando os atributos em inglês e salvando na base dessa forma, peço desculpa pelo deslise.
	
	
